import unittest
import validpass

class TestPass(unittest.TestCase):

    def test_bool_pass(self):
        expected = bool
        result = type(validpass.validpass('abcd1234'))
        self.assertEqual(result, expected)

    def test_generic_pass(self):
        expected = True
        result = validpass.validpass('abcd1234!')
        self.assertEqual(result, expected)

    def test_generic_fail(self):
        expected = False
        result = validpass.validpass('abcd1234')
        self.assertEqual(result, expected)

    def test_len_short_fail(self):
        expected = False
        result = validpass.valid_len('abc123')
        self.assertEqual(result, expected) 

    def test_len_long_fail(self):
        expected = False
        result = validpass.valid_len('abc123abc123a')
        self.assertEqual(result, expected)

    def test_len_pass(self):
        expected = True
        result = validpass.valid_len('abc123abc123')
        self.assertEqual(result, expected)

    def test_has_alpha_fail(self):
        expected = False
        result = validpass.contains_letter('12345678')
        self.assertEqual(result, expected)

    def test_has_alpha_pass(self):
        expected = True
        result = validpass.contains_letter('1234a5678')
        self.assertEqual(result, expected)

    def test_has_num_fail(self):
        expected = False
        result = validpass.contains_num('abcdefgh')
        self.assertEqual(result, expected)

    def test_has_num_true(self):
        expected = True
        result = validpass.contains_num('abcd1efgh')
        self.assertEqual(result, expected)

    
    def test_has_special_fail(self):
        expected = False
        result = validpass.contains_special('abcd1efgh')
        self.assertEqual(result, expected)

    def test_has_special_pass(self):
        expected = True
        result = validpass.contains_special('abcd1efgh$')
        self.assertEqual(result, expected)

