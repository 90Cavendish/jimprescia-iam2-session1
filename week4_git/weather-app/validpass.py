def validpass(password):
    if valid_len(password) and contains_letter(password) and contains_num(password) and contains_special(password):
        return True
    else:
        return False


def valid_len(password):
    if 7 < len(password) < 13:
        return True
    else:
        return False
    

def contains_letter(password):
    for char in password:
        if char.isalpha():
            return True
    else:
        return False


def contains_num(password):
    for char in password:
        if char.isnumeric():
            return True
    else:
        return False

def contains_special(password):
    specials = list('~`!@#$%^&*()[]\\{?<>,.:;}+_')
    for char in password:
        if char in specials:
            return True
    else:
        return False

