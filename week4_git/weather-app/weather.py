#! /usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY = "d21b17405d692b8977dd9098c59754eb"


def get_location():
    """Returns the longitude and latitude for the location of this machine.
    Returns:
    str: longitude
    str: latitude
    """
    req = requests.get('https://ipvigilante.com').json()
    # print(req)
    longi = req['data']['longitude']
    lat = req['data']['latitude']
    # print(long,lat)
    return longi, lat


def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str):
    latitude (str):

    Returns:
    float: temperature
     """
    dark_response = requests.get('https://api.darksky.net/forecast/d21b17405d692b8977dd9098c59754eb/' + latitude + ',' + longitude)
    print(dark_response.json())
    tempterature = dark_response.json()['currently']['temperature']
    return tempterature


def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """
    print("Today's forecast is: " + str(temp))


if __name__ == "__main__":
    longitude, latitude = get_location()
    print(longitude, latitude)
    temp = get_temperature(longitude, latitude)
    print_forecast(temp)
